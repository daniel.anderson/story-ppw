from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from django.http import HttpRequest
# Create your tests here.
class webTest(TestCase):

    def test_landing_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_landing_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed( response, 'index.html')

    def test_landing_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)