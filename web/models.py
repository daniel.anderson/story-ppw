from django.db import models
from django.utils import dates
# Create your models here.

class Schedule(models.Model):
    name = models.CharField(max_length=200, default='')
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=200,default='')
    category = models.CharField(max_length=200,default='')