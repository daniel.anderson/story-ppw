# Generated by Django 2.1.1 on 2018-10-03 14:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='location',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='name',
            field=models.CharField(default='', max_length=200),
        ),
    ]
