from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ScheduleForm
from .models import Schedule

# Create your views here.
response = {}
def index(request):
    return render(request,'index.html')
def regis(request):    
    return render(request,'Regis.html',{'regiskey' : ScheduleForm})
def display(request):
    return render(request,'display.html',{'displaykey': Schedule.objects.all()})
def get(request):
    form = ScheduleForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != '' else 'kosong'
        response['date'] = request.POST['date'] if request.POST['date'] != '' else 'kosong'
        response['time'] = request.POST['time'] if request.POST['time'] != '' else 'kosong'
        response['location'] = request.POST['location'] if request.POST['location'] != '' else 'kosong'
        response['category'] = request.POST['category'] if request.POST['category'] != '' else 'kosong'
        schedule = Schedule(name=response['name'],date=response['date'],time=response['time'],location=response['location'],category=response['category'])
        schedule.save()
        return HttpResponseRedirect('/display/')
    else:
        return HttpResponseRedirect('')
def delete(request):
    for jadwal in Schedule.objects.all():
        jadwal.delete()
    return HttpResponseRedirect('/display/')

    