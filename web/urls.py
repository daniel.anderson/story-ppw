from django.urls import path
from .views import *
urlpatterns = [
    path('', index, name='index'),
    path('regis/',regis,name='regis'),
    path('submit/',get, name='get'),
    path('display/',display, name='display'),
    path('delete/',delete, name = 'delete'),
]