from django import forms

class ScheduleForm(forms.Form):
    attribute = {
        'class' : 'form-control'
    }
    date_attrs = {
        'class' : 'form-control',
        'type': 'date'
    }
    time_attrs = {
        'class' : 'form-control',
        'type' : 'time'
    }

    name = forms.CharField(label='Activity',required = True, max_length=200, widget =forms.TextInput(attrs=attribute))
    date = forms.DateField(label='Date', required = True,widget = forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label = "Time", required = True, widget =forms.TimeInput(attrs=time_attrs))
    location = forms.CharField(label='Location',required= True, max_length=200,widget= forms.TextInput(attrs=attribute))
    category = forms.CharField(label='Category',required = True, max_length=200, widget =forms.TextInput(attrs=attribute))