from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
# Create your tests here.
class webTest(TestCase):

    def test_landing_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_landing_using_buku_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed( response, 'buku.html')

    def test_landing_using_get_buku_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, get_buku)

